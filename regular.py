import random
import re


def remove_brackets_regular_expression(string):
    """
    Метод для обработки строки с помощью регулярного выражения

    :param string: обрабатываемая строка
    :return: обработанная строка
    """

    return re.search(r'.*.*?\)[a-zA-Z0-9]*', string).group(0)


def remove_brackets(string):
    """
    Метод для удаления незакрытых скобок и их содержимого из конца строки.
    Вернет форматированную строку.

    :param string: исходная строка
    :return: строка с удаленными незакрытыми скобками
        и их содержимым из конца строки
    """

    # допустим всё ок
    corrected_string = string

    # проверим так ли всё хорошо на самом деле и зайдем в цикл если ошиблись
    while have_unclosed_brackets(corrected_string):

        # узнаем до какого элемента можно копировать строку
        brackets_left = get_unclosed_brackets_indexes(corrected_string)

        # скопируем до крайних правых открытых скобок
        corrected_string = ''.join(corrected_string[:brackets_left])

    return corrected_string


def have_unclosed_brackets(string):
    """
    Метод для тестов. Проверяет есть ли в строке незакрытые скобки.

    :param string: строка которую будем проверять на наличие незакрытых скобок
    :return:    True - если в строке есть незакрытые скобки
                False - если в строке все скобки закрыты
    """

    # узнаем индексы незакрытых скобок в строке
    brackets_left = get_unclosed_brackets_indexes(string)

    if brackets_left < len(string):
        return True

    return False


def get_unclosed_brackets_indexes(string):
    """
    Метод для определения есть ли в конце строки незакрытые скобки.

    :param string: строка которую проверяем
    :return: индекс последней незакрытой скобки или длина строки
                                            если скобки в конце строки закрыты
    """

    open_brackets, close_brackets = get_brackets_indexes(string)
    if open_brackets:
        max_index_of_open_brackets = max(open_brackets)

        if max_index_of_open_brackets > max(close_brackets):
            return max_index_of_open_brackets

    return len(string)


def get_brackets_indexes(string):
    """
    Метод для получения индексов всех открытых скобок и закрытых скобок в стоке

    :param string: строка в которой ищем закрытые и открытые скобки
    :return: два списка с индексами по которым расположены
                        открытые и закрытые скобки
    """

    open_brackets = []
    close_brackets = []

    for index, char in enumerate(string):
        if char == '(':
            open_brackets.append(index)
        if char == ')':
            close_brackets.append(index)

    return open_brackets, close_brackets


def generate_brackets(string_length):
    """
    Метод для генерации строки случайно расположенных скобок

    :param string_length: максимальная длина генерируемой строки
    :return: строка содержащая перемешанные открытые и закрытые скобки
    """

    brackets_string = ''
    for char_index in range(0, string_length):
        brackets_string += random.choice(['(', ')', 'a'])

    return brackets_string


if __name__ == '__main__':
    number_of_iteration = 100
    max_input_string_length = 30
    for iteration in range(0, number_of_iteration):
        input_string = generate_brackets(max_input_string_length)
        corrected_string = remove_brackets(input_string)
        corrected_string_regular = remove_brackets_regular_expression(input_string)
        print('input string          : {}'.format(input_string))
        print('corrected string      : {}'.format(corrected_string))
        print('by regular expression : {}'.format(corrected_string_regular))
        print('unclosed backets      ? {}'.format(
            have_unclosed_brackets(corrected_string))
        )
        print('\n\n')

    # тесты так себе
    input_string_1 = '()()asfajdgg(asd'
    input_string_2 = ')()(UIASHBFHA()SCHA(VS'
    corrected_string_1 = '()()asfajdgg'
    corrected_string_2 = ')()(UIASHBFHA()SCHA'
    assert remove_brackets(input_string_1) == corrected_string_1
    assert remove_brackets_regular_expression(input_string_1) == corrected_string_1
    assert have_unclosed_brackets(input_string_1) == True
    assert have_unclosed_brackets(corrected_string_1) == False
    assert remove_brackets(input_string_2) == corrected_string_2
    assert remove_brackets_regular_expression(input_string_2) == corrected_string_2
    assert have_unclosed_brackets(input_string_2) == True
    assert have_unclosed_brackets(corrected_string_2) == False
