class Category(models.Model):
    name = models.CharField('Группа товара', max_length=64)

class Product(models.Model):
    category = models.ForeignKey(Category, verbose_name='Группа')
    name = models.CharField('Название товара', max_length=128)
    price = models.DecimalField('Стоимость единицы, руб.', max_digits=10, decimal_places=2)

def task_a(minimum_price):
    """
    Метод для выбора товаров из БД цена которых не ниже minimum_price,
    группировка подходящих товаров в список словарей по категориям.

    :param minimum_price: минимально допустимая цена
    :return: список словарей продуктов разбитых по категориям
    """

    categories_dict = Product.objects.filter(
        price__gte=minimum_price
    ).values(
        'category'
    ).annotate(
        products_count=Count('category')
    )

    for category in categories_dict:
        print('{}  {}'.format(category.category, category.products_count))

    return categories_dict


def task_b(minimum_price, minimum_number_of_products):
    """
    Метод для получения товаров, которые стоят не дешевле minimum_price,
    группировка этих товаров по категориям, и выбор категорий товаров,
    которые содержат более чем minimum_number_of_products продуктов.

    :param minimum_price: минимальная цена продуктов
    :param minimum_number_of_products: количество товаров,
                                    больше которого должно быть в категории
    :return: обработанный список категорий, удовлетворяющих требованиям
                лучшебы тут был кверисет
    """

    categories_dict = task_a(minimum_price)
    TMP = []
    for category in categories_dict:
        if category.products_count > minimum_number_of_products:
            TMP.append(category)

    return TMP


def task_c():
    """
    Метод для вывода в консоль информации обо всех товарах в БД.
    """

    all_products = Product.objects.select_related(
        'category_name'
    ).all().values()

    for product in all_products:
        print('{}  {}  {}'.format(
            product['category'], product['name'], product['price']
        ))


# цена по условию не меньше 100р
minimum_price = 100
# количество товаров в категории более 10
minimum_number_of_products = 10

print(task_a(minimum_price))
print(task_b(minimum_price, minimum_number_of_products))
task_c()
